from flask_restful import Resource
from webargs import fields, validate
from webargs.flaskparser import use_args, use_kwargs, parser, abort
import json

import requests
from bs4 import BeautifulSoup
import json


class RecetteResource(Resource):
    """Details of recipe."""


    recette_args = {"url": fields.Str(required=True)}

    @use_args(recette_args,location="query")
    def get(self, args):
    
        response = requests.get(args["url"])

        soupBis = BeautifulSoup(response.text, 'html.parser')

        dict_recette_detail = {}

        temps,difficulte,cout = [a.string for a in soupBis.find_all('p',attrs={"class":"RCP__sc-1qnswg8-1 iDYkZP"})]
        dict_recette_detail["temps"] = temps
        dict_recette_detail["difficulte"] = difficulte
        dict_recette_detail["cout"] = cout

        dict_details = json.loads(soupBis.find("script",attrs={"type":"application/ld+json"}).contents[0].string)

        interesting_keys = ['recipeCategory','image','totalTime','recipeIngredient','recipeInstructions']
        dict_details = {key:dict_details[key] for key in interesting_keys}
        dict_details['totalTime'] = dict_details['totalTime'].replace("PT","").replace("M"," mins")
        dict_details['recipeInstructions'] = [instruction["text"].replace("\n","") for instruction in dict_details['recipeInstructions'] ] 
        dict_recette_detail.update(dict_details)

        return(dict_recette_detail)