from flask import Flask
from flask_restful import Api, Resource

from webargs import fields, validate
from webargs.flaskparser import use_args, use_kwargs, parser, abort
from flask_cors import CORS


from menu_ressource import MenuResource
from recette_ressource import RecetteResource
from random_ressource import RandomResource
from user_ressource import UserResource


app = Flask(__name__)
CORS(app)
api = Api(app)


class IndexResource(Resource):
    """A welcome page."""

    hello_args = {"name": fields.Str(missing="Friend")}

    @use_args(hello_args,location="query")
    def get(self, args):
        return {"message": "Welcome, {}!".format(args["name"])}




@parser.error_handler
def handle_request_parsing_error(err, req, schema, *, error_status_code, error_headers):
    """webargs error handler that uses Flask-RESTful's abort function to return
    a JSON error response to the client.
    """
    abort(error_status_code, errors=err.messages)

api.add_resource(IndexResource, "/")
api.add_resource(MenuResource, "/api/v1/menu")
api.add_resource(RecetteResource, "/api/v1/recette")
api.add_resource(RandomResource, "/api/v1/random")
api.add_resource(UserResource, "/api/v1/user")

if __name__ == "__main__":
    app.run(port=5001, debug=True)