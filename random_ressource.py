from flask_restful import Resource
from webargs import fields, validate
from webargs.flaskparser import use_args, use_kwargs, parser, abort
import json

import requests
from bs4 import BeautifulSoup
import json
import random
import math as m



class RandomResource(Resource):
    """List of recipe."""


    menu_args = {
    "nb_menu":fields.Int(missing=7)
    }
    
    @use_args(menu_args,location="query")
    def get(self, args):
        root_marmitton = 'https://www.marmiton.org'
        all_recette = []

        num_page = 1
        random_page = [random.randint(1,90) for i in range(m.ceil(args["nb_menu"]/12))]
        for num_page in  random_page:
            succed=False
            while not succed:
                try:
                    args["page"] = num_page
                    search_url = f'{root_marmitton}/recettes/recherche.aspx'
                    response = requests.get(search_url,params=args)
                    succed=True
                except:
                    pass                

            soup = BeautifulSoup(response.text, 'html.parser')
            list_recette = soup.find_all('a',class_="MRTN__sc-1gofnyi-2 gACiYG")
            for recette in list_recette:
                try:
                    dict_recette = {}

                    url_recette = root_marmitton + recette.get("href")
                    nom_recette = recette.find('h4',class_="MRTN__sc-30rwkm-0 dJvfhM").string

                    note_recette = float(recette.find("span",attrs={"class":"SHRD__sc-10plygc-0 jHwZwD"}).text.split("/")[0])
                    img_recette_jpeg = recette.find('picture').find("source",attrs = {"type":"image/jpeg"}).get("srcset")
                    img_recette_webp = recette.find('picture').find("source",attrs = {"type":"image/webp"}).get("srcset")
                    img_recette_dic = {img.rstrip().lstrip().split(" ")[1]:img.rstrip().lstrip().split(" ")[0] for img in img_recette_jpeg.split(",")}

                    dict_recette["url"] = url_recette
                    dict_recette["nom"] = nom_recette
                    dict_recette["note"] = note_recette
                    dict_recette["img"] = {}
                    dict_recette["img"]["jpeg"] = img_recette_jpeg
                    dict_recette["img"]["webp"] = img_recette_webp
                    dict_recette["img"]["width"] = img_recette_dic

                    all_recette.append(dict_recette)
                except:
                    pass
                
        all_recette = all_recette[:args["nb_menu"]]

        return {"count":len(all_recette),"recettes":all_recette}