from flask_restful import Resource
from marshmallow import missing
from webargs import fields, validate
from webargs.flaskparser import use_args, use_kwargs, parser, abort
import json

import requests
from bs4 import BeautifulSoup
import json
import random
import os





with open("marmitton_config.json") as f:
    marmiton_config = json.load(f)

class UserResource(Resource):
    """List of recipe."""

    # with open("user_template.json") as f2:
    #     user_template = json.load(f2)

    with open("user.json") as f:
        dict_users = json.load(f)


    get_args = {"ip": fields.Str(required=False),
    "favorie": fields.Str(),
    "dt": fields.Str(validate=lambda p: all([para in list(marmiton_config["Type de plat"]["values"].values()) for para in p.split(",")]) ),
    "dif": fields.Str(validate=lambda p: all([para in list(marmiton_config["Difficulté"]["values"].values()) for para in p.split(",")]) ),
    "exp": fields.Str(validate=lambda p: all([para in list(marmiton_config["Coût"]["values"].values()) for para in p.split(",")]) ),
    "prt": fields.Str(validate=lambda p: all([para in list(marmiton_config["Régime"]["values"].values()) for para in p.split(",")]) ),
    "rct": fields.Str(validate=lambda p: all([para in list(marmiton_config["Materiel"]["values"].values()) for para in p.split(",")]) ),
    "ttlt": fields.Str(validate=lambda p: all([para in list(marmiton_config["Temps de cuisine"]["values"].values()) for para in p.split(",")]) ), 
    }

    @use_args(get_args,location="query")
    def get(self, args):
        if "ip" not in args.keys():
            return(self.dict_users)
        try:
            # Check if user is in dict
            match_user = [i  for i in range(len(self.dict_users["users"])) if self.dict_users["users"][i]["id_user"] == args["ip"]]
            if len(match_user) == 0:

                with open("user_template.json") as f2:
                    user_template = json.load(f2)

                user_template["id_user"] = args["ip"]

                self.dict_users["users"].append(user_template)

                return(user_template)
            else:
                ind_user = match_user[0]
                if "favorie" in args.keys():
                    self.dict_users["users"][ind_user]["recette_favorie"] = args["favorie"].split(",")

                for param in self.dict_users["users"][ind_user]["recette_param"].keys():
                    if self.dict_users["users"][ind_user]["recette_param"][param]["url_para"] in args.keys():
                        for value in self.dict_users["users"][ind_user]["recette_param"][param]["values"].keys():
                            self.dict_users["users"][ind_user]["recette_param"][param]["values"][value] = value in args[self.dict_users["users"][ind_user]["recette_param"][param]["url_para"]].split(",")



                return(self.dict_users["users"][ind_user])

        except Exception as e:
            return({"erreur": e})
            
