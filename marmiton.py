from sys import prefix
import time
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from bs4 import BeautifulSoup
import json
import itertools
import os
import warnings
import random
warnings.filterwarnings("ignore")



# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()

def get_recette(**args):
    filter_url = "&".join([f'{config_marmiton[list(config_marmiton)[i]]["url_para"]}={config_marmiton[list(config_marmiton)[i]]["values"][c]}' for i,c in enumerate(config)])

def get_recette_details(recette_url):
    response = requests.get(recette_url)

    soupBis = BeautifulSoup(response.text, 'html.parser')

    dict_recette_detail = {}

    temps,difficulte,cout = [a.string for a in soupBis.find_all('p',attrs={"class":"RCP__sc-1qnswg8-1 iDYkZP"})]#;print(time.time()-t0)
    dict_recette_detail["temps"] = temps
    dict_recette_detail["difficulte"] = difficulte
    dict_recette_detail["cout"] = cout

    dict_details = json.loads(soupBis.find("script",attrs={"type":"application/ld+json"}).contents[0].string)

    interesting_keys = ['recipeCategory','image','totalTime','recipeIngredient','recipeInstructions']
    dict_details = {key:dict_details[key] for key in interesting_keys}
    dict_details['totalTime'] = dict_details['totalTime'].replace("PT","").replace("M"," mins")
    dict_details['recipeInstructions'] = [instruction["text"].replace("\n","") for instruction in dict_details['recipeInstructions'] ] 
    dict_recette_detail.update(dict_details)

    return(dict_recette_detail)




with open('marmitton_config.json', 'r') as f:
    config_marmiton = json.load(f)

root_marmitton = 'https://www.marmiton.org'


all_recette = []

if "all_config.json" in os.listdir("scraping"):
    with open("scraping/all_config.json",encoding='utf8') as f:
        all_config= json.load(f)
else:
    all_config = list(itertools.product(*[list(conf["values"]) for _,conf in config_marmiton.items()]))
    random.shuffle(all_config)
    with open("scraping/all_config.json","w") as f:
        json.dump(all_config,f)


nb_erreur = 0

debut = 1228
for i,config in enumerate(all_config[debut:]):
    printProgressBar(debut+i, len(all_config), prefix = f'Progress (config:{debut+i}-recette:{len(all_recette)}) - erreur({nb_erreur}):',decimals = 3)
    filter_url = "&".join([f'{config_marmiton[list(config_marmiton)[i]]["url_para"]}={config_marmiton[list(config_marmiton)[i]]["values"][c]}' for i,c in enumerate(config)])

    next_page = True
    num_page = 1
    while next_page:
        succed=False
        while not succed:
            try:
                printProgressBar(debut+i, len(all_config), prefix = f'Progress (config:{debut+i}-recette:{len(all_recette)}) - erreur({nb_erreur}):',decimals = 3,suffix="New session")
                session = requests.Session()
                retry = Retry(connect=20, backoff_factor=2)
                adapter = HTTPAdapter(max_retries=retry)
                session.mount('http://', adapter)
                session.mount('https://', adapter)
                search_url = f'{root_marmitton}/recettes/recherche.aspx?aqt=-&page={num_page}&{filter_url}'
                response = session.get(search_url)
                succed=True
            except:
                printProgressBar(debut+i, len(all_config), prefix = f'Progress (config:{debut+i}-recette:{len(all_recette)}) - erreur({nb_erreur}):',decimals = 3,suffix="Sleeping")
                time.sleep(15*60)
            

        soup = BeautifulSoup(response.text, 'html.parser')
        list_recette = soup.find_all('a',class_="MRTN__sc-1gofnyi-2 gACiYG")
        for recette in list_recette:
            printProgressBar(debut+i, len(all_config), prefix = f'Progress (config:{debut+i}-recette:{len(all_recette)}) - erreur({nb_erreur}):',decimals = 3,suffix="New recette")
            try:
                dict_recette = {"type":config[0],"difficulte":config[1],"cout":config[2],"regime":config[3],"materiel":config[4],"temps":config[5]}

                url_recette = root_marmitton +recette.get("href")
                nom_recette = recette.find('h4',class_="MRTN__sc-30rwkm-0 dJvfhM").string
                img_recette = recette.find('picture').find("source",attrs = {"type":"image/jpeg"}).get("srcset")
                img_recette = {img.rstrip().lstrip().split(" ")[1]:img.rstrip().lstrip().split(" ")[0] for img in img_recette.split(",")}

                dict_recette["url"] = url_recette
                dict_recette["nom"] = nom_recette
                dict_recette["img"] = img_recette

                dict_recette["recette"] = get_recette_bs(url_recette,i=debut+i,len_all_config=len(all_config),len_all_recette=len(all_recette),nb_erreur=nb_erreur)


                all_recette.append(dict_recette)
            except:
                nb_erreur += 1
            finally:
                nb_erreur += 1 
        
        if len(list_recette) > 0 and num_page <= 20:
            num_page += 1
        else:
            next_page = False

        if i%50 == 0:
            printProgressBar(debut+i, len(all_config), prefix = f'Progress (config:{debut+i}-recette:{len(all_recette)}) - erreur({nb_erreur}):',decimals = 3,suffix="Save JSON")
            try:
                with open('public/data/recette.json', 'w',encoding='utf8') as f:
                    json.dump(all_recette, f)
            except:
                pass