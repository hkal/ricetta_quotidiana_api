from flask_restful import Resource
from marshmallow import missing
from webargs import fields, validate
from webargs.flaskparser import use_args, use_kwargs, parser, abort
import json

import requests
from bs4 import BeautifulSoup
import json
import random




with open("marmitton_config.json") as f:
    marmiton_config = json.load(f)

class MenuResource(Resource):
    """List of recipe."""
    # validate=lambda p: all([par in list(marmiton_config["Difficulté"]["values"].values()) for par in p.split(",")]) 


    menu_args = {"dt": fields.Str(validate=lambda p: all([para in list(marmiton_config["Type de plat"]["values"].values()) for para in p.split(",")]) ),
    "dif": fields.Str(validate=lambda p: all([para in list(marmiton_config["Difficulté"]["values"].values()) for para in p.split(",")]) ),
    "exp": fields.Str(validate=lambda p: all([para in list(marmiton_config["Coût"]["values"].values()) for para in p.split(",")]) ),
    "prt": fields.Str(validate=lambda p: all([para in list(marmiton_config["Régime"]["values"].values()) for para in p.split(",")]) ),
    "rct": fields.Str(validate=lambda p: all([para in list(marmiton_config["Materiel"]["values"].values()) for para in p.split(",")]) ),
    "ttlt": fields.Str(validate=lambda p: all([para in list(marmiton_config["Temps de cuisine"]["values"].values()) for para in p.split(",")]) ),
    "nb_random": fields.Int(missing=None),
    "max_page":fields.Int(missing=5)    
    }

    @use_args(menu_args,location="query")
    def get(self, args):
        root_marmitton = 'https://www.marmiton.org'
        all_recette = []
        print(args)
        next_page = True
        num_page = 1
        nb_req = 0
        max_nb_recette = args["nb_random"] if args["nb_random"] else 120
        while next_page:
            nb_recette_keep = random.randint(0,args["nb_random"] - len(all_recette)) if args["nb_random"] else 12
            if nb_recette_keep == 0:
                num_page += 1
            else:
                nb_req += 1
                succed=False
                while not succed:
                    try:
                        args["page"] = num_page
                        search_url = f'{root_marmitton}/recettes/recherche.aspx?{"&".join([key+"="+val for key,value in args.items() for val in str(value).split(",") if key!="max_page"])}'
                        # print("&".join([str(key)+"="+str(value) for key,value in args.items()]))
                        # print(args)
                        response = requests.get(search_url)
                        # print(response.url)
                        succed=True
                    except Exception as e:
                        print(e)                

                soup = BeautifulSoup(response.text, 'html.parser')
                list_recette = soup.find_all('a',class_="MRTN__sc-1gofnyi-2 gACiYG")
                list_recette = [list_recette[i] for i in random.sample(range(0,12),nb_recette_keep)] if args["nb_random"] else list_recette
                for recette in list_recette:
                    try:
                        dict_recette = {}

                        url_recette = root_marmitton + recette.get("href")
                        nom_recette = recette.find('h4',class_="MRTN__sc-30rwkm-0 dJvfhM").string

                        note_recette = float(recette.find("span",attrs={"class":"SHRD__sc-10plygc-0 jHwZwD"}).text.split("/")[0])
                        img_recette_jpeg = recette.find('picture').find("source",attrs = {"type":"image/jpeg"}).get("srcset")
                        img_recette_webp = recette.find('picture').find("source",attrs = {"type":"image/webp"}).get("srcset")
                        img_recette_dic = {img.rstrip().lstrip().split(" ")[1]:img.rstrip().lstrip().split(" ")[0] for img in img_recette_jpeg.split(",")}

                        dict_recette["url"] = url_recette
                        dict_recette["nom"] = nom_recette
                        dict_recette["note"] = note_recette
                        dict_recette["img"] = {}
                        dict_recette["img"]["jpeg"] = img_recette_jpeg
                        dict_recette["img"]["webp"] = img_recette_webp
                        dict_recette["img"]["width"] = img_recette_dic

                        all_recette.append(dict_recette)
                    except:
                        pass
                # print(nb_req)
                if len(list_recette) > 0 and nb_req < args["max_page"]:
                    num_page += 1
                else:
                    next_page = False

            if len(all_recette) >= max_nb_recette:
                next_page = False

        return {"count":len(all_recette),"recettes":all_recette}